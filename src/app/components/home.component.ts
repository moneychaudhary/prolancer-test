import {Component, OnDestroy, OnInit} from '@angular/core';
import {HttpService} from "../services/http-service";
import {Data} from "../models/data";
import {Observable} from "rxjs/Rx";


@Component({
  selector: 'app-home-component',
  template: `
    <div fxLayoutAlign="center center" fxFlex="100%" fxLayout="column" fxLayoutGap="20px" >
    <div class="loader" *ngIf="!loaded"></div>
    <ngx-charts-bar-vertical
      *ngIf="loaded"
      [view]="view"
      [scheme]="colorScheme"
      [results]="statsData"
      [xAxis]="showXAxis"
      [yAxis]="showYAxis"
      [showXAxisLabel]="showXAxisLabel"
      [showYAxisLabel]="showYAxisLabel"
      [xAxisLabel]="xAxisLabel"
      [yAxisLabel]="yAxisLabel">
    </ngx-charts-bar-vertical>
    <div *ngIf="updating">UPDATING DATA.....</div>  
    </div>
    
  `,
  styles: [`

    .loader {
      border: 6px solid #f3f3f3; /* Light grey */
      border-top: 6px solid #0b1f2d; /* Blue */
      border-radius: 50%;
      width: 80px;
      height: 80px;
      margin-top: 200px;
      animation: spin 2s linear infinite;
    }

    @keyframes spin {
      0% {
        transform: rotate(0deg);
      }
      100% {
        transform: rotate(360deg);
      }
    }

    .center {
      margin-left: auto;
      margin-right: auto;
    }
  `]
})
export class HomeComponent implements OnInit, OnDestroy {



  statsData=[];

  loaded=false;
  updating=false;

  view: any[] = [800, 500];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showXAxisLabel = true;
  xAxisLabel = 'Currencies';
  showYAxisLabel = true;
  yAxisLabel = 'Price in ($)';

  isAlive = true;

  private  time = 60*1000*10;

  colorScheme = {
    domain: ['#5AA454', '#A10A28', '#C7B42C', '#AAAAAA']
  };

  constructor(private service: HttpService) {

  }
  ngOnInit(): void {

    this.updateData();
    Observable.interval(this.time).takeWhile(() => true).subscribe(x => {
      this.updating = true;
      this.updateData();
    });

  }

  private updateData()
  {
    console.log('called');
    this.service.getData({limit: 10}).subscribe((res: Data[]) => {
        if(res) {
          this.statsData = [];
          res.forEach(res => {
            this.statsData.push({
              name:res.name,
              value:res.price_usd
            });
          });
          this.loaded = true;
          this.updating =false;
        }
      },
      error => console.log(error));
  }

  ngOnDestroy(): void {
    this.isAlive = false;
  }

}
