
export class Data {
  id: string;
  name: string;
  symbol: string;
  rank: number;
  price_usd: string;
  price_btc: string;
}
