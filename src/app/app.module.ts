import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import {HomeComponent} from "./components/home.component";
import {HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {HttpModule} from "@angular/http";
import {HttpService} from "./services/http-service";
import {NgxChartsModule} from "@swimlane/ngx-charts";
import {MatProgressSpinnerModule} from "@angular/material";
import {FlexLayoutModule} from "@angular/flex-layout";

@NgModule({
  declarations: [
    HomeComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    BrowserAnimationsModule,
    NgxChartsModule,
    FlexLayoutModule
  ],
  providers: [HttpService],
  bootstrap: [HomeComponent]
})
export class AppModule { }
