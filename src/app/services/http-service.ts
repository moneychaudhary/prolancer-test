import {Injectable} from '@angular/core';
import {HttpClient, HttpParams} from '@angular/common/http';


@Injectable()
export class HttpService {

  /**
   * Constants
   */
  private BASE_URL = 'https://api.coinmarketcap.com/v1';



  getData(data: { limit: number}): any {
    return this.get('/ticker',data);
  }



  constructor(private http: HttpClient) {
  }

  private get(url: string, data?: any) {

    const params = new HttpParams().set('limit',data.limit);

    return this.http.get(this.BASE_URL + url, {
      params:params
    });
  }


  private handleError(error: Response): any {
    console.log('[error] start');
    console.log(error);
    console.log('[error] end');
  }

}
